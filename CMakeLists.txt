cmake_minimum_required(VERSION 2.8.3)
project(acl_msgs)

find_package(catkin REQUIRED COMPONENTS 
    message_generation 
    std_msgs 
    geometry_msgs
    nav_msgs
    actionlib_msgs)

add_message_files(
  DIRECTORY msg
  FILES
  GPparams.msg
  JoyDef.msg
  QuadCmd.msg
  QuadGoal.msg
  QuadHealth.msg
  QuadPath.msg
  QuadPathArray.msg
  QuadRawSensors.msg
  Waypoint.msg
  Trajectory.msg
  CarGoal.msg
  CarCmd.msg
  PolyInputs.msg
  SimpleState.msg
  CarState.msg
  VehicleList.msg
  QuadWaypoint.msg
  QuadWaypointError.msg
  ViconState.msg
  )

## Generate services in the 'srv' folder
add_service_files(
  FILES
  GenPath.srv
  GenSimplePath.srv
  MultiVehPreCompute.srv
  MultiVehTakeoffLand.srv
  MultiVehWaypoint.srv
  OLcmd.srv
  ResetCar.srv
  ReturnToBase.srv
  SingleSegment.srv
  SingleTrack.srv
  RunStep.srv
  PilcoRollout.srv
)

## Generate actions in the 'action' folder
add_action_files(
  FILES
  BroadcastTrajectory.action
  Return2Start.action
)

generate_messages(DEPENDENCIES std_msgs geometry_msgs nav_msgs actionlib_msgs)

catkin_package(CATKIN_DEPENDS message_runtime std_msgs geometry_msgs nav_msgs actionlib_msgs)
